#!/usr/bin/python

import os
import re
from pathlib import Path
from TestResize import CreateThumbnail2
from multiprocessing import Pool
import time
import datetime
#from joblib import Parallel, delayed

# start editable vars #
outputfile	= ".\\inventory.txt"	# file to save the results to
folderSrc		= ".\\images"		# the folder to inventory
folderDest		= ".\\thumbnail"		# the folder to inventory
exclude		= ['Thumbs.db','.tmp', 'thumbnail']	# exclude files containing these strings
targetedExtensions = ['.jpg','.jpeg','.png'] # Only these files will be listed
pathsep		= "\\"			# path seperator ('/' for linux, '\' for Windows)
# end editable vars #
rePatternExt = '.*(' + '|'.join(str(ext) for ext in targetedExtensions) + ')$'
#printAndWrite(txtFile,'Regex Pattern for File Extensions : ' + rePatternExt)
regTestExtensions = re.compile(rePatternExt, re.I)

#functions
def printAndWrite(txtFile, msg):
    print(msg)
    txtFile.write("\n" + datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S") + " - " + msg)

def lastSegments(path, nbSegments):
    ret = ""
    pathSplit = path.split(pathsep)
    i = 0
    while(i < nbSegments and len(pathSplit) > i):
        ret = pathSplit[len(pathSplit) - i - 1] + pathsep + ret
        i += 1
    return ret

def makeFolderEndWithSeparator(path):
    if path[len(path)-1] == pathsep:
        return path
    else: 
        return path + pathsep

def getThumbnailPath(srcPath):
    ret = makeFolderEndWithSeparator(folderDest) + srcPath[len(folderSrc):]
    return ret

def thumbnailarize(pFolderSrc, pFolderDest, size):
    filesToThumbnalirize = []
    global folderSrc  
    global folderDest 
    folderSrc = makeFolderEndWithSeparator(pFolderSrc)
    folderDest = makeFolderEndWithSeparator(pFolderDest)
    startingTime = time.time()

    with open(outputfile, "a+") as txtFile:
        for path,dirs,files in os.walk(folderSrc):
            #sep = "\n---------- " + lastSegments(path,3000) + " ----------"
            #path.split(pathsep)[len(path.split(pathsep))-1]
            #printAndWrite(txtFile,sep)
            #txtfile.write("%s\n" % sep)
            if not any(x in path for x in exclude):
                for fn in sorted(files):
                    if not any(x in fn for x in exclude) and regTestExtensions.match(fn):
                        filename = fn#os.path.splitext(fn)[0]
                        fullFileName = makeFolderEndWithSeparator(path) + filename
                        #txtfile.write("%s\n" % filename)
                        if not Path(getThumbnailPath(fullFileName)).is_file():
                            #printAndWrite(txtFile,filename)
                            filesToThumbnalirize.append(fullFileName)
                        #else:
                            #printAndWrite(txtFile,"Already thumbnail-ized : " + fullFileName)
                    #else:
                        #printAndWrite(txtFile,'Not matched : ' + fn)

        proccessedCounter = 0
        progTotal = len(filesToThumbnalirize)

        if(len(filesToThumbnalirize) > 0): 
            try:
                printAndWrite(txtFile,"Starting resizing for " + str(progTotal) + " files in parallel with " + str(os.cpu_count()) + " threads")
                resizePool = Pool(min(len(filesToThumbnalirize),os.cpu_count()))
                parameters = []
                for file in filesToThumbnalirize:
                    #proccessedCounter+=1
                    #print ("Processing file " + str(proccessedCounter) + "/" + str(progTotal) + " - " + file)
                    parameters.append([file,getThumbnailPath(file), size])
                    #CreateThumbnail2([file, getThumbnailPath(file), 1270])
                p = resizePool.map(CreateThumbnail2, parameters)
                #printAndWrite(txtFile,"Time needed per image : ")
                #print (p)
            except Exception as e: 
                printAndWrite(txtFile,"Error : ")
                print (e)
            finally: 
                printAndWrite(txtFile, "Time spent per images (-1 are errors): ")
                printAndWrite(txtFile, str(p))
                resizePool.close()
                resizePool.join()
                
        printAndWrite(txtFile,"Resizing Task is DONE using " +str( time.time() - startingTime) + " sec")
        
        txtFile.close()
#main*

if __name__ == '__main__':
    #tumbnailarize(folderSrc,folderDest)
    #thumbnailarize('D:\\pic2018Export\\Juin\\Brunch_Traversier','.\\thumbT5')
    thumbnailarize("D:\\pic2018Export", "D:\\pic2018Export_thumbnails\\h_350", 350)
    thumbnailarize("D:\\pic2018Export", "D:\\pic2018Export_thumbnails\\h_1080", 1080)

#Brunch_Traversier
