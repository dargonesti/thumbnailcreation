#!/usr/bin/python
from PIL import Image
import os
from resizeimage import resizeimage
#import os
import pathlib
import time

#if not os.path.exists(directory):
#    os.makedirs(directory)


pathlib.Path('.\\thumbnail').mkdir(parents=True, exist_ok=True)
pathlib.Path('.\\cover').mkdir(parents=True, exist_ok=True)
 
def CreateThumbnail2(args):
    try:
        startTime = time.time()
        CreateThumbnail(args[0],args[1],args[2])
        return time.time()-startTime
    
    except Exception as e:  
        print (e)
        return -1

def CreateThumbnail(fileSrc, fileDest, size):
    #print(fileSrc)
    print("Creating : " + fileDest)
    #print(str(size))
    newFoldName = os.path.dirname(fileDest)
    pathlib.Path(newFoldName).mkdir(parents=True, exist_ok=True)
    with open(fileSrc, 'r+b') as f:
        with Image.open(f) as image:
            if image.size[1] <= size:
                img = image
            else:                    
                img = resizeimage.resize_height(image,min(size, image.size[1]))
            # resize_thumbnail(image, [size, size])
            img.save(fileDest, image.format)
        f.close()
