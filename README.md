thumbnailcreation

Creates thumbnails of all images under folderSRC.
Creates the thumbnails in heights 350 and 1080 ( or keeps the original sizes for smaller files ).
The default paths are the ones I uses, you would need to change them in listFiles.py for your own purposes.

Images are resized on cpu using multithread, resizing on GPU is an option, but the bottleneck is the hdd anyway so there wouldn't be any time gain.

On my pc, with an i7 6700k and a single HDD for the input / output. 
it took 6 minutes to create 1300 thumbnails of both sizes. 
Then it takes 1 second to scan and see if any thumbnails needs to ba added. 

On my setup, 
    this scripts is scheduled to run every few hours,
    together with an other one that creates HTML tags targeting each images and thumbnails to create a gallery.
    But use it as you will.