#!/usr/bin/python
from PIL import Image
import os
from resizeimage import resizeimage
#import os
import pathlib
import time

#if not os.path.exists(directory):
#    os.makedirs(directory)


pathlib.Path('.\\thumbnail').mkdir(parents=True, exist_ok=True)
pathlib.Path('.\\cover').mkdir(parents=True, exist_ok=True)

with open('3_DSCF6385.jpg', 'r+b') as f:
    with Image.open(f) as image:
        cover = resizeimage.resize_cover(image, [1270, 1270])
        cover.save('.\\cover\\test-image-cover1.jpg', image.format)
        img = resizeimage.resize_thumbnail(image, [1270, 1270])
        img.save('.\\thumbnail\\test-image-thumbnail1.jpg', image.format)
    f.close()

with open('3_DSCF6222.jpg', 'r+b') as f:
    with Image.open(f) as image:
        cover = resizeimage.resize_cover(image, [1270, 1270])
        cover.save('.\\cover\\test-image-cover2.jpg', image.format)
        img = resizeimage.resize_thumbnail(image, [1270, 1270])
        img.save('.\\thumbnail\\test-image-thumbnail2.jpg', image.format)
    f.close()
def CreateThumbnail2(args):
    startTime = time.time()
    CreateThumbnail(args[0],args[1],args[2])
    return time.time()-startTime

def CreateThumbnail(fileSrc, fileDest, size):
    #print(fileSrc)
    print("Creating : " + fileDest)
    #print(str(size))
    newFoldName = os.path.dirname(fileDest)
    pathlib.Path(newFoldName).mkdir(parents=True, exist_ok=True)
    with open(fileSrc, 'r+b') as f:
        with Image.open(f) as image:
            img = resizeimage.resize_thumbnail(image, [size, size])
            img.save(fileDest, image.format)
        f.close()

#TODO : Save to subfolder with same image name ( And create folder )
#TODO : Multithread this shitz

#TODO : Check for all images in target folder and for already present images in subfolder 